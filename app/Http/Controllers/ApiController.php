<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Http\Requests\AgeRequest;
use App\Models\Bank;
use App\Models\Config;
use App\Models\ConfigBonus;
use App\Models\Activity;
use App\Models\LineRegister;
use App\Models\MapAmphur;
use App\Models\MapDistrict;
use App\Models\MapProvince;
use App\Models\MapZipcode;
use App\Models\Member;
use App\Models\MemberLine;
use App\Models\MemberStock;
use App\Models\OrderWholesale;
use App\Models\Payment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use DB;

class ApiController extends Controller
{
    public function calculateAge(Request $request){

        //dd( 'cal',$request->all() );

        $date_bday = ($request->year_th-543).'-'.($request->month_key+1).'-'.$request->date;
        $date_current = date("Y-m-d");

        $day_num = date("w",strtotime($date_bday));
        $day_texts = Array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");


        $date1 = date_create($date_bday);
        $date2 = date_create($date_current);
        $diff = date_diff($date1,$date2)->format("%a");
        $week = floor($diff/7);
        $day = $diff-($week*7);
//        dd($diff,$week,$day);

        $resault = 'คุณเกิดวัน'.$day_texts[$day_num].' ปัจจุบันคุณมีอายุ '.number_format($week,0).' สัปดาห์ กับอีก '.$day.' วัน';
//dd('b-day',$resault);

        $params = [
            'success'=>true,
            'message'=>'ทำการประมวลผลสำเร็จ',
            'resault'=>$resault,
        ];
        return response()->json($params, 200);
    }
}