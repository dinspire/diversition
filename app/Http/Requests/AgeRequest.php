<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|numeric',
            'month_key' => 'required|numeric',
            'year_th' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'โปรเลือกวันที่',
            'date.numeric' => 'โปรเลือกวันที่',
            'month_key.required' => 'โปรเลือกเดือน',
            'month_key.numeric' => 'โปรเลือกเดือน',
            "year_th.required" =>  'โปรเลือกปี',
            "year_th.numeric" =>  'โปรเลือกปี',
        ];
    }
}
