<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>diversition test</title>

        {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
        <link href="{{ asset("/css/app.css") }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset("/bower_components/angular-material/angular-material.css") }}">
        <link rel='stylesheet' href="{{ asset("/bower_components/angular-loading-bar/build/loading-bar.min.css")}}" type='text/css' media='all' />
        <link href="{{ asset("/bower_components/angular-ui-notification/dist/angular-ui-notification.min.css") }}" rel="stylesheet" type="text/css" />

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
        </style>
    </head>
    <body  ng-app="YourApp">
        <div class="container" ng-controller="MainController" ng-cloak="">
            <h3>Diversition Test</h3>
            <h4>ระบบคำนวนอายุ ด้วยวันเดือนปีเกิด</h4>
            {{--<div class="flex-center position-ref full-height">--}}
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">แบบฟอร์ม</div>
                    <div class="panel-body">
                        <form role="form" id="form"  name="form" ng-submit="submit()">
                            <p>
                                กรอกวันเดือนปีเกิด
                            </p>
                            <div layout="row" layout-align="start" flex>
                                <md-input-container flex="25">
                                    <label>วันที่</label>
                                    <md-select name="date_select" ng-model="dates.date" required>
                                        <md-option ng-repeat="d in date_num_selects" value="@{{d}}">@{{ d }}</md-option>
                                    </md-select>
                                    <div ng-messages="form.date_select.$error" role="alert" multiple>
                                        <div ng-message="required" class="my-message">โปรดเลือกวันที่</div>
                                    </div>
                                </md-input-container>
                                <md-input-container flex="50">
                                    <label>เดือน</label>
                                    <md-select name="month_select" ng-model="dates.month_key" required>
                                        <md-option ng-repeat="m in month_selects" ng-value="$index">@{{ m }}</md-option>
                                    </md-select>
                                    <div ng-messages="form.date_select.$error" role="alert" multiple>
                                        <div ng-message="required" class="my-message">โปรดเลือกเดือน</div>
                                    </div>
                                </md-input-container>
                                <md-input-container flex="25">
                                    <label>ปี พ.ศ.</label>
                                    <md-select name="year_select" ng-model="dates.year_th" required>
                                        <md-option ng-repeat="y in year_selects" ng-value="y">@{{ y }}</md-option>
                                    </md-select>
                                    <div ng-messages="form.year_select.$error" role="alert" multiple>
                                        <div ng-message="required" class="my-message">โปรดเลือกปี</div>
                                    </div>
                                </md-input-container>
                            </div>

                            <section layout="row" layout-sm="column" layout-align="center end" layout-wrap>
                                <md-button class="md-raised md-primary" type="submit">คำนวน</md-button>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">ผลลัพธ์</div>
                    <div class="panel-body">
                        <p ng-show="dates.date == 0 || dates.month_key==null || dates.year_th == 0">วันเกิดของคุณ คือ - </p>
                        <p ng-show="dates.date > 0 && dates.month_key && dates.year_th > 0">วันเกิดของคุณ คือ @{{ dates.date + ' ' + month_selects[dates.month_key] + ' ' + dates.year_th }}</p>
                        <p>ผลลัพ  (ณ วันที่ @{{ current_date_th }})</p>
                        <p ng-show="resault && resault!=''">
                            <div class="alert alert-success">@{{ resault }}</div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {{--</div>--}}

        <script src="{{ asset("/bower_components/angular/angular.js") }}"></script>
        <script src="{{ asset("/bower_components/angular-aria/angular-aria.js") }}"></script>
        <script src="{{ asset("/bower_components/angular-animate/angular-animate.js") }}"></script>
        <script src="{{ asset("/bower_components/angular-messages/angular-messages.js") }}"></script>
        <script src="{{ asset("/bower_components/angular-material/angular-material.js") }}"></script>
        <script src="{{ asset("/bower_components/angular-loading-bar/build/loading-bar.min.js") }}"></script>
        <script src="{{ asset("/bower_components/angular-ui-notification/dist/angular-ui-notification.js") }}"></script>

        <script>
            // Include app dependency on ngMaterial
            angular.module( 'YourApp', [ 'ngMaterial', 'ngMessages', 'angular-loading-bar','ui-notification' ] )
                .config(function(NotificationProvider) {
                    NotificationProvider.setOptions({
                        maxCount:3,
                        positionX: 'right',
                        positionY: 'bottom',
                    });
                })
                .factory('Main', function($http, $httpParamSerializerJQLike) {
                    return {
                        calculateAge : function (data, method) {
                            if (method != undefined) {
                                data._method = method;
                            }
                            return $http({
                                method: "POST",
                                url: 'api/cal_age',
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                data: $httpParamSerializerJQLike(data)
                            });//.error(onFail);
                        },
                    }
                })
                .controller("MainController", function ($scope,Main,Notification) {
                    var date_num = [];
                    for(var d=1;d<=31;d++){
                        date_num.push(d);
                    }
                    $scope.date_num_selects = date_num;

                    $scope.month_selects = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

                    var d = new Date();
                    var year_current = d.getFullYear()+543;
                    $scope.year_selects = [];
                    for(var y=1;y<=100;y++){
                        $scope.year_selects.push(year_current--);
                    }

                    $scope.current_date_th = d.getDate()+' '+$scope.month_selects[d.getMonth()]+' '+(d.getFullYear()+543);
                    $scope.dates = {
                        date : 0,
                        month_key : null,
                        year_th : 0,
                    };

                    $scope.resault = '';
                    $scope.submit = function () {
                        if( $scope.dates.date == 0 || $scope.dates.month_key==null || $scope.dates.year_th == 0 ){
                            return false;
                        }
                        Main.calculateAge($scope.dates).then(
                            function (resp) {
                                console.log('resp',resp);
                                if (resp && resp.data && resp.data.success) {
                                    $scope.resault = resp.data.resault;
                                    Notification.success({title:'success',message: resp.data.message});
                                }else{
                                    console.error('error',resp);
                                    Notification.error({title:'error',message: resp.data.message});
                                }
                            },
                            function (err) {
                                console.error('error',err);
                                Notification.error({title:'error',message: resp.data.message});
                            }
                        );
                    }
                });
        </script>
    </body>
</html>
